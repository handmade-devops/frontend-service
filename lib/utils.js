export const fetcher = async (endpoint, { body, ...customConfig } = {}) => {
  const headers = body ? { "Content-Type": "application/json" } : {};
  const config = {
    ...customConfig,
    headers: {
      ...headers,
      ...customConfig.headers,
    },
  };
  if (body) config.body = JSON.stringify(body);

  try {
    const response = await fetch(endpoint, config);

    let data = null;
    try {
      data = await response.text();
      data = JSON.parse(data);
    } catch (err) {
      data = JSON.parse("null");
    }

    if (response.status === 401 || response.ok) return data;

    const error = new Error(response.statusText);
    error.response = response;
    error.data = data;
    throw error;
  } catch (error) {
    if (!error.data) error.data = { message: error.message };
    throw error;
  }
};

export const styles = {
  page:
    "flex flex-col items-center justify-center w-full min-h-screen px-8 pt-32 pb-16 bg-gray-50",
  composedPage:
    "relative flex flex-col items-center justify-center min-h-screen gap-24 px-4 pt-32 pb-16 overflow-hidden text-gray-800 bg-purple-800 min-w-screen lg:px-8",
  card:
    "relative w-full max-w-6xl p-8 mx-auto text-gray-800 bg-white rounded shadow-xl lg:p-20 lg:text-left",
  cardH1: "text-2xl font-bold",
  grid:
    "grid w-full max-w-6xl grid-cols-1 gap-4 my-8 lg:grid-cols-3 md:grid-cols-2",
  cardGridHeader: "text-2xl font-bold text-center lg:text-4xl mt-4",
  input:
    "w-full px-4 py-2 my-2 bg-white border border-gray-300 rounded-md ring-purple-600 focus:ring-4 focus:border-transparent ring-offset-2 focus:outline-none",
};

export const isSeller = (user) => user?.user_role === "SELLER";
export const isBuyer = (user) => user?.user_role === "BUYER";

export default { fetcher, styles, isBuyer, isSeller };
