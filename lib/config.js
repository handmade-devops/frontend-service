const DEV = "development";
const NODE_ENV = process.env.NODE_ENV || DEV;

console.log({ env: process.env });

const apiPort = process.env.API_PORT || 8000;
export const baseUrl = `http://${
  NODE_ENV === DEV ? "localhost" : process.env.BASE_HOST
}:${apiPort}/api`;
export const apiUrl = `http://${process.env.API_HOST}:${apiPort}/api`;

console.log({ baseUrl, apiUrl });

export default { baseUrl, apiUrl };
