import { withIronSession } from "next-iron-session";
import { getSecret } from "docker-secret";

const password =
  process.env.NODE_ENV === "development"
    ? process.env.SECRET_COOKIE_PASSWORD
    : getSecret(process.env.SECRET_COOKIE_PASSWORD_FILE);

console.log({ password });

export default function withSession(handler) {
  return withIronSession(handler, {
    password,
    cookieName: "handmade-login",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false,
    },
  });
}
