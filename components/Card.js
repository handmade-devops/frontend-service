import Link from "next/link";
import Button from "./Button";

const Card = ({
  id,
  name,
  description,
  logo_path,
  linkTo = "products",
  query = "",
}) => {
  return (
    <div className="overflow-hidden border-2 border-purple-600 rounded-lg">
      <div className="relative overflow-hidden bg-gradient-to-br from-purple-200 to-purple-300 pb-52">
        <img
          className="absolute object-cover object-center w-full h-full origin-center transform scale-105"
          src={logo_path}
          alt=""
        />
      </div>
      <div className="flex h-[calc(100%-13em)]">
        <div className="flex flex-col flex-grow h-full px-8 py-10">
          <h3 className="text-2xl font-bold text-gray-700">{name}</h3>
          <p className="font-bold leading-7 text-gray-600">{description}</p>
          <div className="flex flex-col items-end justify-end flex-grow mt-8">
            <Link href={`/${linkTo}/${id}${query}`} passHref>
              <Button
                variant="purple-gradient"
                size="small"
                className="flex-grow-0 tracking-wide"
              >
                Read more
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
