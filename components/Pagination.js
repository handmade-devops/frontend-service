import Arrow from "./Arrow";

const Pagination = ({ pageIndex, setPageIndex }) => (
  <div className="flex flex-row gap-4">
    <span className="relative block w-48">
      <button
        onClick={() => (pageIndex > 0 ? setPageIndex(pageIndex - 1) : 0)}
        className={`absolute right-0 flex items-center px-4 py-2 font-bold transition-colors border-2  rounded-full hover:text-gray-50 focus:outline-none ring-offset-2 ${
          pageIndex === 0
            ? "text-gray-600 border-gray-600 hover:text-gray-600 hover:cursor-default"
            : "text-purple-600 border-purple-600 hover:bg-purple-600 focus:ring-2"
        }`}
      >
        <span className="transform rotate-180">
          <Arrow />
        </span>
        Previous page
      </button>
    </span>
    <span className="relative block w-48">
      <button
        onClick={() => setPageIndex(pageIndex + 1)}
        className="absolute left-0 flex items-center justify-around px-4 py-2 font-bold text-white transition-colors bg-purple-600 border-2 border-purple-600 rounded-full hover:bg-gray-50 hover:text-purple-600 focus:outline-none focus:ring-2 ring-offset-2"
      >
        Next page
        <Arrow />
      </button>
    </span>
  </div>
);

export default Pagination;
