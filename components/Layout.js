import Link from 'next/link';
import useUser from "../lib/useUser";
import { useState } from "react";

const Layout = ({ children }) => {
  const { user } = useUser();
  const [navOpen, setNavOpen] = useState(false);

  return (
    <div className="w-full min-h-screen">
      <nav className="fixed top-0 left-0 z-30 flex items-center justify-between w-full px-4 py-2 text-white bg-purple-800 lg:px-8 md:justify-start">
        <div className="block mr-auto md:hidden">
          <button
            className={`p-2 text-gray-300 focus:outline-none transform transition-transform focus:text-white ${
              navOpen ? "rotate-0" : "rotate-45"
            }`}
            onClick={() => setNavOpen(!navOpen)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5 fill-current"
              viewBox="0 0 24 24"
            >
              <path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z" />
            </svg>
          </button>
        </div>
        <div className="flex mx-auto space-x-2 text-xl font-bold tracking-wider md:mx-0 hover:cursor-pointer">
          <Link href="/">Handmade Devops</Link>
        </div>
        <ul
          className={`absolute left-0 right-0 md:flex flex-col px-2 space-y-1 text-gray-300 pb-4 md:pb-0 z-10 bg-purple-800 md:space-x-2 md:px-0 md:space-y-0 md:ml-4 md:relative top-full md:flex-row ${
            navOpen ? "flex" : "hidden"
          }`}
        >
          <li>
            <Link href="/organisations">
              <a className="block px-4 py-2 font-semibold rounded-lg hover:bg-purple-600">
                Organisations
              </a>
            </Link>
          </li>
          <li>
            <Link href="/campaigns">
              <a className="block px-4 py-2 font-semibold rounded-lg hover:bg-purple-600">
                Campaigns
              </a>
            </Link>
          </li>
          <li>
            <Link href="/products">
              <a className="block px-4 py-2 font-semibold rounded-lg hover:bg-purple-600">
                Products
              </a>
            </Link>
          </li>
        </ul>

        <div className="flex items-center ml-auto space-x-2">
          {user && user.isLoggedIn ? (
            <Link href="/profile">
              <a className="block px-4 py-2 font-semibold text-gray-300 rounded-lg hover:bg-purple-600">
                {user.username}
              </a>
            </Link>
          ) : (
            <Link href="/login">
              <a className="block px-4 py-2 font-semibold text-gray-300 rounded-lg hover:bg-purple-600">
                Login
              </a>
            </Link>
          )}
        </div>
      </nav>

      <>{children}</>
    </div>
  );
}

export default Layout;
