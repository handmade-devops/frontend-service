const Progress = ({radius=42, percent=75}) => {
  const circumference = radius * 2 * Math.PI;

  return (
    <div className="flex flex-wrap items-center h-16 max-w-md px-6 my-5 bg-white border-2 border-purple-500 rounded-2xl">
      <div className="flex items-center justify-center -mt-5 bg-white border-2 border-purple-500 rounded-full">
        <svg
          className="transform translate-x-1 translate-y-1 w-28 h-28"
          aria-hidden="true"
        >
          <circle
            className="text-gray-300"
            strokeWidth="10"
            stroke="currentColor"
            fill="transparent"
            r={radius}
            cx={radius + 10}
            cy={radius + 10}
          />
          <circle
            className="text-green-500"
            strokeWidth="10"
            strokeDasharray={circumference}
            strokeDashoffset={circumference - (percent / 100) * circumference}
            strokeLinecap="round"
            stroke="currentColor"
            fill="transparent"
            r={radius}
            cx={radius + 10}
            cy={radius + 10}
          />
        </svg>
        <span className="absolute text-2xl text-green-500">{percent}%</span>
      </div>
      <p className="ml-6 font-medium text-gray-600 -mt-9 sm:text-xl">
        Completion
      </p>
      <span className="hidden ml-auto text-xl font-medium text-purple-600 -mt-9 sm:block">
        +{percent}%
      </span>
    </div>
  );
};

export default Progress;
