import React from "react";
import Arrow from "./Arrow";

const Button = React.forwardRef(
  (props, ref) => {
    const {
      children,
      variant = "default",
      size = "default",
      noArrow = false,
      className,
      onClick,
      href,
    } = props;
    let colors, sizes;

    switch (variant) {
      case "green":
        colors = "bg-green-500 hover:bg-green-700 ring-green-500";
        break;
      case "purple-gradient":
        colors =
          "bg-gradient-to-r from-purple-600 to-purple-800 hover:from-purple-800";
        break;
      case "red":
        colors = "bg-red-500 hover:bg-red-700 ring-red-500";
        break;
      default:
        colors = "bg-purple-600 hover:bg-purple-800 ring-purple-600";
    }

    switch (size) {
      case "small":
        sizes = "px-4 py-1 text-sm focus:ring-2";
        break;
      default:
        sizes = "px-6 py-3 focus:ring-4";
    }

    return (
      <a
        href={href}
        onClick={onClick}
        ref={ref}
        className={`hover:cursor-pointer inline-block font-bold transition-colors rounded-full text-gray-50 focus:outline-none ring-offset-2 ${colors} ${sizes} ${className}`}
      >
        {children}
        {noArrow ? null : <Arrow {...{ size: size === "small" ? 18 : 24 }} />}
      </a>
    );
  }
);

export default Button;
