FROM node:14.16.0-alpine

WORKDIR /home/node/app

COPY . ./

RUN apk add --no-cache git && npm i && npm run build

CMD ["node_modules/.bin/next", "start", "-p", "$PORT"]
