import format from "date-fns/format";
import parseISO from "date-fns/parseISO";
import Head from "next/head";
import Link from "next/link";
import useSWR from "swr";
import Button from "../components/Button";
import { baseUrl } from "../lib/config";
import { fetcher } from "../lib/utils";

export default function Home() {
  const { data: products, error: productsErr } = useSWR(
    `${baseUrl}/product`,
    fetcher
  );
  const { data: campaigns, error: campaignsErr } = useSWR(
    `${baseUrl}/campaign`,
    fetcher
  );

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-50">
      <Head>
        <title>Handmade DevOps</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="relative w-full bg-red-200 h-96 main-cover">
        <div className="w-full h-full opacity-30 bg-gradient-to-r from-yellow-400 via-purple-500 to-blue-500"></div>
      </div>

      <main className="z-10 flex flex-col items-center flex-1 w-full px-4 -mt-48 text-center lg:px-16">
        <div className="h-48">
          <h1 className="text-4xl font-bold leading-relaxed text-white lg:text-6xl">
            Welcome to
            <br className="block lg:hidden" />
            <span className="p-2 ml-4 text-purple-800 bg-white rounded-md">
              Handmade DevOps
            </span>
          </h1>

          <p className="mt-6 text-2xl text-gray-50 lg:text-4xl">
            Get started by browsing the available offers
          </p>
        </div>

        <div className="flex flex-col items-center justify-around w-full mt-24 mb-48 space-x-0 space-y-4 flex-nowrap lg:space-x-8 lg:space-y-0 lg:flex-row">
          <div className="flex flex-col self-stretch flex-grow p-4 space-y-4 border-2 border-purple-600 max-w-none lg:max-w-3xl rounded-xl">
            {productsErr ? (
              <h2 className="text-2xl font-bold">
                These are not the products you are looking for
              </h2>
            ) : !products ? (
              <h2 className="text-2xl font-bold">Loading products...</h2>
            ) : products.length === 0 ? (
              <h2 className="text-2xl font-bold">
                No products published yet...
              </h2>
            ) : (
              <div className="flex flex-row flex-wrap gap-4">
                {products.slice(0, 2).map((p) => (
                  <Link key={p.id} href={`/products/${p.id}`}>
                    <div className="flex-grow text-left border-2 border-purple-600 rounded-lg hover:cursor-pointer">
                      <div className="w-full rounded-md rounded-b-none bg-gradient-to-br from-purple-200 to-purple-300 h-36"></div>
                      <div className="p-6 -mt-10">
                        <h3 className="pt-2 pl-4 -ml-4 text-2xl font-bold rounded-xl bg-gray-50">
                          {p.name}
                        </h3>
                        <div className="flex flex-row justify-between mt-4">
                          <span className="px-4 py-1 text-sm font-bold text-white bg-green-500 rounded-full">
                            {p.total - p.sold} left
                          </span>
                          <Button variant="purple-gradient" size="small">
                            Buy now!
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Link>
                ))}
              </div>
            )}
            <div>
              <Link href="/products" passHref>
                <Button>Check latest products</Button>
              </Link>
            </div>
          </div>
          <div className="flex flex-col self-stretch flex-grow p-4 space-y-4 border-2 border-purple-600 max-w-none lg:max-w-3xl rounded-xl">
            {campaignsErr ? (
              <h2 className="text-2xl font-bold">
                These are not the campaigns you are looking for
              </h2>
            ) : !campaigns ? (
              <h2 className="text-2xl font-bold">Loading campaigns...</h2>
            ) : campaigns.length === 0 ? (
              <h2 className="text-2xl font-bold">
                No campaigns published yet...
              </h2>
            ) : (
              <div className="flex flex-row flex-wrap gap-4">
                {campaigns.slice(0, 2).map((c) => (
                  <Link key={c.id} href={`/campaigns/${c.id}`}>
                    <div className="flex-grow text-left border-2 border-purple-600 rounded-lg hover:cursor-pointer">
                      <div className="w-full rounded-md rounded-b-none bg-gradient-to-br from-purple-200 to-purple-300 h-36"></div>
                      <div className="p-6 -mt-10">
                        <h3 className="pt-2 pl-4 -ml-4 text-2xl font-bold rounded-xl bg-gray-50">
                          {c.name}
                        </h3>
                        <div className="flex flex-row justify-between mt-4">
                          <span className="px-4 py-1 text-sm font-bold text-white bg-green-500 rounded-full">
                            <time dateTime={c.end_date}>
                              {format(parseISO(c.end_date), "LLLL d, yyyy")}
                            </time>
                          </span>
                          <Button
                            variant="purple-gradient"
                            noArrow
                            size="small"
                          >
                            See more!
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Link>
                ))}
              </div>
            )}
            <div className="">
              <Link href="/campaigns" passHref>
                <Button>Check latest campaigns</Button>
              </Link>
            </div>
          </div>
        </div>
      </main>
      <div className="w-full px-4 py-6 text-white bg-purple-800 lg:px-16">
        Powered by vanntile & carrtile, 2021
      </div>
    </div>
  );
}
