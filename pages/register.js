import { Switch } from "@headlessui/react";
import Head from "next/head";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import Button from "../components/Button";
import useUser from "../lib/useUser";
import { fetcher, styles } from "../lib/utils";

const CustomSwitch = ({ value, onChange }) => (
  <Switch.Group>
    <Switch.Label className="mr-4">I am a *</Switch.Label>
    <div className="flex flex-row items-center gap-2 my-2">
      <span className="px-4 py-1 bg-purple-600 rounded-full text-gray-50">
        Buyer
      </span>
      <Switch
        checked={value}
        onChange={onChange}
        className={`${
          value ? "bg-green-500" : "bg-purple-600"
        } relative inline-flex items-center h-9 w-20 rounded-full`}
      >
        <span className="sr-only">Are you a seller?</span>
        <span
          className={`${
            value ? "translate-x-12" : "translate-x-2"
          } inline-block w-6 h-6 transform transition ease-in-out duration-200 rounded-full bg-white`}
        />
      </Switch>
      <span className="px-4 py-1 bg-green-500 rounded-full text-gray-50">
        Seller
      </span>
    </div>
  </Switch.Group>
);

const Register = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const { mutateUser } = useUser({
    redirectTo: "/profile",
    redirectIfFound: true,
  });
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async ({ username, email, password, user_role }) => {
    try {
      await mutateUser(
        fetcher("/api/register", {
          method: "POST",
          body: {
            username,
            email,
            password,
            user_role: user_role ? "SELLER" : "BUYER",
            organisation_id: null,
          },
        })
      );
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  return (
    <div className={styles.page}>
      <Head>
        <title>Handmade - Register</title>
      </Head>
      <h1 className={styles.cardGridHeader}>We're glad you want to register</h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full max-w-xl p-4 mx-8 mt-4 mb-16 text-gray-800 border-2 border-purple-600 rounded-xl"
      >
        <div className="flex flex-col mb-4 font-bold">
          <label>
            Username *
            <input
              type="text"
              name="name"
              placeholder="username"
              autoComplete="username"
              required
              className={styles.input}
              {...register("username", { required: true })}
            />
          </label>
          <label>
            Email *
            <input
              type="email"
              name="email"
              placeholder="name@acme.com"
              autoComplete="email"
              required
              className={styles.input}
              {...register("email", { required: true })}
            />
          </label>
          <label>
            Your password *
            <input
              type="password"
              name="password"
              placeholder="password"
              autoComplete="new-password"
              minLength="8"
              required
              className={styles.input}
              {...register("password", { required: true, minLength: 8 })}
            />
          </label>
          <Controller
            name="user_role"
            control={control}
            defaultValue={false}
            render={({ field: { value, onChange } }) => (
              <CustomSwitch value={value} onChange={onChange} />
            )}
          />
        </div>

        <div className="font-bold text-red-600">
          <p className="my-4">{errors.username && "Username is required"}</p>
          <p className="my-4">{errors.email && "Email is required"}</p>
          <p className="my-4">
            {errors.password && "Password needs to have a minimum 8 characters"}
          </p>
          <p className="my-4">{errorMsg && errorMsg}</p>
        </div>

        <Button type="submit" onClick={handleSubmit(onSubmit)}>
          Register
        </Button>
      </form>
    </div>
  );
};

export default Register;
