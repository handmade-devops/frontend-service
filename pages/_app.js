import Router from "next/router";
import { done, start } from "nprogress";
import "../styles/globals.css";
import "../styles/nprogress.css";
import Layout from "../components/Layout"

Router.events.on("routeChangeStart", () => start());
Router.events.on("routeChangeComplete", () => done());
Router.events.on("routeChangeError", () => done());

function MyApp({ Component, pageProps }) {
  return <Layout> <Component {...pageProps} /> </Layout >;
}

export default MyApp;
