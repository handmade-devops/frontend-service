import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import useSWR from "swr";
import Button from "../../components/Button";
import Card from "../../components/Card";
import Progress from "../../components/Progress";
import { baseUrl } from "../../lib/config";
import { fetcher, styles } from "../../lib/utils";

const Campaign = () => {
  const router = useRouter();
  const { slug } = router.query;
  const [percent, setPercent] = useState(0);

  const { data: campaign, error } = useSWR(
    slug ? `${baseUrl}/campaign/${slug}` : null,
    fetcher
  );

  const { data: products, error: productsError } = useSWR(
    campaign ? `${baseUrl}/product/campaign/${campaign.id}` : null,
    fetcher,
    {
      onSuccess: (data) => {
        setPercent(
          data.map((e) => e.sold / e.total).reduce((acc, curr) => acc + curr, 0)
        );
      },
    }
  );

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>
          {campaign
            ? `Handmade Devops - ${campaign.name}`
            : "Campaign loading..."}
        </title>
      </Head>
      <div className={styles.card}>
        {error ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : !campaign ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (
          <div className="items-center md:flex">
            <Link href={`/organisations/${campaign.organisation_id}`} passHref>
              <Button
                variant="green"
                size="small"
                className="absolute uppercase top-4 right-4"
              >
                See organisation
              </Button>
            </Link>
            <div className="w-full mb-10 md:px-10 md:w-1/2 md:mb-0">
              <div className="relative min-h-[20em]">
                <img src="" className="relative z-10 w-full" alt="" />
                <div className="absolute z-0 border-4 border-purple-600 top-10 bottom-10 left-10 right-10"></div>
              </div>
            </div>
            <div className="w-full md:px-10 lg:w-1/2">
              <div className="mb-10">
                <h1 className="mb-5 text-2xl font-bold uppercase">
                  {campaign.name}
                </h1>
                <p className="text-sm">{campaign.description}</p>
              </div>
              <Progress percent={percent} />
            </div>
          </div>
        )}
      </div>

      <div className={`${styles.card} max-w-7xl`}>
        <h1 className={styles.cardGridHeader}>Campaign products</h1>
        {productsError ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : !products ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (
          <div className={styles.grid}>
            {products.map((o) => (
              <Card key={o.id} {...o} />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Campaign;
