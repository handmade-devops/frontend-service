import Head from "next/head";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { useState } from "react";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import { Controller, useForm } from "react-hook-form";
import useSWR from "swr";
import Button from "../../../components/Button";
import Card from "../../../components/Card";
import Toast from "../../../components/Toast";
import { baseUrl } from "../../../lib/config";
import useUser from "../../../lib/useUser";
import { fetcher, isBuyer, styles } from "../../../lib/utils";

const EditCampaign = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const [toast, setToast] = useState(false);
  const router = useRouter();
  const { slug } = router.query;
  const isNew = slug === "new";
  const { user } = useUser({ redirectTo: "/login" });
  const {
    control,
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const { data: campaign, error } = useSWR(
    user?.isFull && !isNew ? `${baseUrl}/campaign/${slug}` : null,
    fetcher,
    {
      onSuccess: (data) => {
        setValue("dates", {
          from: data?.start_date ? new Date(data?.start_date) : undefined,
          to: data?.end_date ? new Date(data?.end_date) : undefined,
        });
      },
    }
  );

  const { data: products, error: productsError } = useSWR(
    user?.isFull && !isNew && campaign
      ? `${baseUrl}/product/campaign/${campaign.id}`
      : null,
    fetcher
  );

  const onSubmit = async ({ name, description, dates }) => {
    try {
      const response = await fetcher(
        `${baseUrl}/campaign/${isNew ? "" : slug}`,
        {
          method: isNew ? "POST" : "PUT",
          headers: { Authorization: `Bearer ${user.token}` },
          body: {
            name,
            description,
            start_date: dates.from.toISOString(),
            end_date: dates.to.toISOString(),
          },
        }
      );

      setToast("Data saved successfully.");
      Router.push(`/campaigns/edit/${response.id}`);
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  const deleteCampaign = async () => {
    try {
      await fetcher(`${baseUrl}/campaign/${slug}`, {
        method: "DELETE",
        headers: { Authorization: `Bearer ${user.token}` },
      });

      setToast("Data deleted successfully.");
      Router.push(`/profile`);
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  if (user && !user?.isLoggedIn) {
    Router.push("/login");
  } else if (isBuyer(user) || user?.organisation_id === null) {
    Router.push("/profile");
  }

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>Handmade Devops - {isNew ? "Create" : "Edit"} campaign</title>
      </Head>
      <div className={styles.card}>
        {!user || !user.isFull ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : isNew || campaign ? (
          <>
            <h1 className={styles.cardH1}>
              {isNew ? "Create" : "Edit"} campaign
            </h1>
            {!isNew && (
              <Link
                href={`/products/edit/new?campaign=${campaign.id}`}
                passHref
              >
                <Button
                  variant="green"
                  size="small"
                  className="absolute uppercase top-4 right-4"
                >
                  Create new product
                </Button>
              </Link>
            )}
            <form onSubmit={handleSubmit(onSubmit)} className="w-full my-4">
              <div className="flex flex-col mb-4 font-bold">
                <label>
                  Name *
                  <input
                    type="text"
                    name="name"
                    placeholder="name"
                    defaultValue={isNew ? "" : campaign.name}
                    required
                    className={styles.input}
                    {...register("name", { required: true })}
                  />
                </label>
                <label>
                  Description
                  <textarea
                    type="text"
                    name="description"
                    placeholder="description"
                    defaultValue={isNew ? "" : campaign.description}
                    className={styles.input}
                    {...register("description")}
                  />
                </label>

                <div>Campaign interval *</div>
                <Controller
                  name="dates"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { value, onChange } }) => (
                    <div>
                      <div>
                        {value?.from && value?.to
                          ? `${value.from.toLocaleDateString()} - ${value.to.toLocaleDateString()}`
                          : ""}
                      </div>
                      <DayPicker
                        className="Selectable"
                        todayButton="Go to Today"
                        selectedDays={value}
                        disabledDays={[{ before: new Date() }]}
                        onDayClick={(day, modifiers = {}) => {
                          if (modifiers.disabled) return;
                          onChange(DateUtils.addDayToRange(day, value));
                        }}
                      />
                    </div>
                  )}
                />
              </div>

              <div className="font-bold text-red-600">
                <p className="my-4">{errors.name && "Name is required"}</p>
                <p className="my-4">
                  {errors.dates && "Start and end dates are required"}
                </p>
                <p className="my-4">{errorMsg && errorMsg}</p>
              </div>
              <Toast message={toast} show={toast} setShow={setToast} />

              <div className="flex flex-row flex-wrap justify-between gap-4">
                <Button type="submit" onClick={handleSubmit(onSubmit)}>
                  Save
                </Button>

                {!isNew && (
                  <Button onClick={deleteCampaign} variant="red" noArrow>
                    Delete
                  </Button>
                )}
              </div>
            </form>
          </>
        ) : error ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : (
          <h1 className={styles.cardH1}>Loading...</h1>
        )}
      </div>
      {user?.isLoggedIn && !isNew && campaign && (
        <div className={`${styles.card} max-w-7xl`}>
          {productsError ? (
            <div>We couldn't find your campaigns</div>
          ) : !products ? (
            <h1 className={styles.cardH1}>Loading...</h1>
          ) : (
            <>
              <h1 className={styles.cardGridHeader}>Edit products</h1>
              <div className={styles.grid}>
                {products.map((o) => (
                  <Card
                    key={o.id}
                    {...o}
                    linkTo="products/edit"
                    query={`?campaign=${campaign.id}`}
                  />
                ))}
              </div>
            </>
          )}
        </div>
      )}
      <style>{`
        .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--outside) {
          background-color: #C4B5FD !important;
          color: #5b21b6;
        }
        .Selectable .DayPicker-Day {
          border-radius: 0 !important;
        }
      `}</style>
    </div>
  );
};

export default EditCampaign;
