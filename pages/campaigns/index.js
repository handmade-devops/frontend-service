import Head from "next/head";
import { useState } from "react";
import useSWR from "swr";
import Card from "../../components/Card";
import Pagination from "../../components/Pagination";
import { baseUrl } from "../../lib/config";
import { fetcher, styles } from "../../lib/utils";

export default function Campaigns() {
  const [pageIndex, setPageIndex] = useState(0);
  const { data: campaigns, error } = useSWR(
    `${baseUrl}/campaign?page=${pageIndex}`,
    fetcher
  );

  return (
    <div className={styles.page}>
      <Head>
        <title>See our campaigns</title>
      </Head>
      {error ? (
        <h1 className={styles.cardH1}>An error has occured</h1>
      ) : !campaigns ? (
        <h1 className={styles.cardH1}>Loading...</h1>
      ) : (
        <>
          <h1 className={styles.cardGridHeader}>Our campaigns</h1>
          <div className={styles.grid}>
            {campaigns.map((o) => (
              <Card key={o.id} {...o} linkTo="campaigns" />
            ))}
          </div>
          <Pagination pageIndex={pageIndex} setPageIndex={setPageIndex} />
        </>
      )}
    </div>
  );
}
