import Head from "next/head";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { useState } from "react";
import useSWR from "swr";
import Button from "../../components/Button";
import { baseUrl } from "../../lib/config";
import useUser from "../../lib/useUser";
import { fetcher, isBuyer, styles } from "../../lib/utils";

const Product = () => {
  const router = useRouter();
  const { slug } = router.query;
  const { user } = useUser();
  const [errorMsg, setErrorMsg] = useState("");
  const [count, setCount] = useState(1);

  const { data: product, error } = useSWR(
    slug ? `${baseUrl}/product/${slug}` : null,
    fetcher
  );

  const onProductCountChange = (e) => {
    e.preventDefault();
    let value = parseInt(e.target.value);

    if (isNaN(value)) {
      setCount("");
      setErrorMsg("Invalid amount");
    } else if (value < 1) {
      setCount(value);
      setErrorMsg("Minimum amount is 1");
    } else if (value <= product.total - product.sold) {
      setCount(value);
      setErrorMsg(null);
    } else {
      setErrorMsg("Maximum available items");
    }
  };

  const addToBasket = async () => {
    try {
      if (!errorMsg) {
        await fetcher(`${baseUrl}/basket/`, {
          method: "POST",
          headers: { Authorization: `Bearer ${user.token}` },
          body: {
            product_id: product.id,
            count,
          },
        });

        Router.push(`/profile`);
      }
    } catch (err) {
      setErrorMsg(err.data.message);
    }
  };

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>
          {product ? `Handmade Devops - ${product.name}` : "Product loading..."}
        </title>
      </Head>
      <div className={styles.card}>
        {error ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : !product ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (
          <div className="items-center md:flex">
            <Link href={`/campaigns/${product.campaign_id}`} passHref>
              <Button
                variant="green"
                size="small"
                className="absolute uppercase top-4 right-4"
              >
                See campaign
              </Button>
            </Link>
            <div className="w-full mb-10 md:px-10 md:w-1/2 md:mb-0">
              <div className="relative min-h-[20em]">
                <img src="" className="relative z-10 w-full" alt="" />
                <div className="absolute z-0 border-4 border-purple-600 top-10 bottom-10 left-10 right-10"></div>
              </div>
            </div>
            <div className="w-full md:px-10 md:w-1/2">
              <div className="mb-10">
                <h1 className="text-2xl font-bold uppercase">{product.name}</h1>
                <div className="mb-5 text-gray-600 text-small">
                  {product.total - product.sold} available
                </div>
                <p className="text-sm">{product.description}</p>
              </div>
              <div>
                {isBuyer(user) &&
                  (product.sold < product.total ? (
                    <>
                      <div className="font-bold text-red-600">
                        <p className="my-4">{errorMsg && errorMsg}</p>
                      </div>
                      <div className="flex flex-col items-center gap-2">
                        <label>
                          <span className="">Please select quantity</span>
                          <input
                            type="number"
                            name="quantity"
                            step="1"
                            required
                            className={`${styles.input}`}
                            value={count}
                            onChange={onProductCountChange}
                          />
                        </label>
                        <div className="flex flex-row items-center justify-center gap-4">
                          <div className="inline-block mr-5 font-bold align-bottom">
                            <span className="text-2xl leading-none align-baseline">
                              $
                            </span>
                            <span className="text-4xl leading-none align-baseline">
                              {Math.floor(product.price * count * 100) / 100}
                            </span>
                          </div>
                          <Button
                            onClick={addToBasket}
                            className={
                              errorMsg
                                ? "bg-gray-600 hover:cursor-normal hover:gray-600"
                                : ""
                            }
                          >
                            Add to basket
                          </Button>
                        </div>
                      </div>
                    </>
                  ) : (
                    <div className="inline-block px-6 py-3 font-bold transition-colors bg-purple-900 rounded-full ring-4 ring-current text-purple-50">
                      Sold out
                    </div>
                  ))}
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Product;
