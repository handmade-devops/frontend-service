import { Listbox, Transition } from "@headlessui/react";
import Head from "next/head";
import Router, { useRouter } from "next/router";
import { Fragment, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import useSWR from "swr";
import Button from "../../../components/Button";
import Toast from "../../../components/Toast";
import { baseUrl } from "../../../lib/config";
import useUser from "../../../lib/useUser";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { fetcher, isBuyer, styles } from "../../../lib/utils";

const CustomList = ({ value, onChange, campaigns }) => (
  <div className="max-w-full pb-2 w-96">
    <Listbox {...{ value, onChange }}>
      {({ open }) => (
        <>
          <Listbox.Label>Campaign</Listbox.Label>
          <div className="relative z-20 mt-1">
            <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white border border-gray-300 rounded-lg cursor-default focus:outline-none focus-visible:ring-4 ring-purple-600 focus-visible:border-transparent">
              <span className="block truncate">{value?.name}</span>
              <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <SelectorIcon
                  className="w-5 h-5 text-gray-400"
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options
                static
                className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none"
              >
                {campaigns.map((c) => (
                  <Listbox.Option
                    key={c.id}
                    className={({ active }) =>
                      `${
                        active
                          ? "text-purple-800 bg-purple-200"
                          : "text-gray-800"
                      } cursor-default select-none relative py-2 pl-10 pr-4`
                    }
                    value={c}
                  >
                    {({ selected, active }) => (
                      <>
                        <span className="block font-normal truncate">
                          {c.name}
                        </span>
                        {selected && (
                          <span
                            className={`${
                              active ? "text-purple-600" : "text-gray-600"
                            } absolute inset-y-0 left-0 flex items-center pl-3`}
                          >
                            <CheckIcon className="w-5 h-5" aria-hidden="true" />
                          </span>
                        )}
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  </div>
);

const EditProduct = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const [toast, setToast] = useState(false);
  const router = useRouter();
  const { slug, campaign: campaignId } = router.query;
  const isNew = slug === "new";
  const { user } = useUser({ redirectTo: "/login" });
  const {
    control,
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const { data: product, error: productErr } = useSWR(
    user?.isFull && !isNew ? `${baseUrl}/product/${slug}` : null,
    fetcher
  );

  const { data: campaigns, error: campaignsErr } = useSWR(
    user?.isFull && user.organisation_id && (!isNew || !campaignId)
      ? `${baseUrl}/campaign/organisation/${user.organisation_id}`
      : null,
    fetcher,
    {
      onSuccess: (data) => setValue("campaign", data[0]),
    }
  );

  const { data: productCampaign, errors: productCampaignErr } = useSWR(
    user?.isFull && user.organisation_id && isNew && campaignId
      ? `${baseUrl}/campaign/${campaignId}`
      : null,
    fetcher,
    {
      onSuccess: (data) => setValue("campaign", data),
    }
  );

  const onSubmit = async ({ name, description, campaign, price, total }) => {
    try {
      const response = await fetcher(
        `${baseUrl}/product/${isNew ? "" : slug}`,
        {
          method: isNew ? "POST" : "PUT",
          headers: { Authorization: `Bearer ${user.token}` },
          body: {
            name,
            campaign_id: campaign.id,
            description,
            total,
            price,
          },
        }
      );

      setToast("Data saved successfully.");
      Router.push(
        `/products/edit/${response.id}?campaign=${response.campaign_id}`
      );
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  const deleteProduct = async () => {
    try {
      await fetcher(`${baseUrl}/product/${slug}`, {
        method: "DELETE",
        headers: { Authorization: `Bearer ${user.token}` },
      });

      setToast("Data deleted successfully.");
      Router.push(`/campaigns/edit/${product.campaign_id}`);
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  if (user && !user?.isLoggedIn) {
    Router.push("/login");
  } else if (isBuyer(user) || user?.organisation_id === null) {
    Router.push("/profile");
  }

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>Handmade Devops - {isNew ? "Create" : "Edit"} product</title>
      </Head>
      <div className={styles.card}>
        {!user || !user.isFull ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (isNew && productCampaign) || (product && campaigns) ? (
          <>
            <h1 className={styles.cardH1}>
              {isNew ? "Create" : "Edit"} product
            </h1>
            <form onSubmit={handleSubmit(onSubmit)} className="w-full my-4">
              <div className="flex flex-col mb-4 font-bold">
                <label>
                  Name *
                  <input
                    type="text"
                    name="name"
                    placeholder="name"
                    defaultValue={isNew ? "" : product.name}
                    required
                    className={styles.input}
                    {...register("name", { required: true })}
                  />
                </label>
                <label>
                  Description
                  <textarea
                    type="text"
                    name="description"
                    placeholder="description"
                    defaultValue={isNew ? "" : product.description}
                    className={styles.input}
                    {...register("description")}
                  />
                </label>
                <Controller
                  name="campaign"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { value, onChange } }) => (
                    <CustomList
                      {...{
                        value,
                        onChange,
                        campaigns:
                          isNew && campaignId ? [productCampaign] : campaigns,
                      }}
                    />
                  )}
                />
                <label>
                  <span className="block w-full">Total *</span>
                  <input
                    type="number"
                    name="total"
                    placeholder="total"
                    defaultValue={isNew ? 0 : product.total}
                    required
                    className={`${styles.input} max-w-full w-96`}
                    {...register("total", {
                      required: true,
                      valueAsNumber: true,
                      min: 1,
                    })}
                  />
                </label>
                <label>
                  <span className="block w-full">Price *</span>
                  <input
                    type="number"
                    name="total"
                    placeholder="total"
                    step="0.1"
                    defaultValue={isNew ? 0 : product.price}
                    required
                    className={`${styles.input} max-w-full w-96`}
                    {...register("price", {
                      required: true,
                      valueAsNumber: true,
                      min: 0,
                    })}
                  />
                </label>
              </div>

              <div className="font-bold text-red-600">
                <p className="my-4">{errors.name && "Name is required"}</p>
                <p className="my-4">
                  {errors.total && "Total needs to be at least 1"}
                </p>
                <p className="my-4">{errorMsg && errorMsg}</p>
              </div>
              <Toast message={toast} show={toast} setShow={setToast} />

              <div className="flex flex-row flex-wrap justify-between gap-4">
                <Button type="submit" onClick={handleSubmit(onSubmit)}>
                  Save
                </Button>

                {!isNew && (
                  <Button onClick={deleteProduct} variant="red" noArrow>
                    Delete
                  </Button>
                )}
              </div>
            </form>
          </>
        ) : productErr || campaignsErr || productCampaignErr ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : (
          <h1 className={styles.cardH1}>Loading...</h1>
        )}
      </div>
    </div>
  );
};

export default EditProduct;
