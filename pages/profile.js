import { TrashIcon } from "@heroicons/react/solid";
import Head from "next/head";
import Link from "next/link";
import Router from "next/router";
import { useState } from "react";
import useSWR, {mutate} from "swr";
import Button from "../components/Button";
import Card from "../components/Card";
import Toast from "../components/Toast";
import { baseUrl } from "../lib/config";
import useUser from "../lib/useUser";
import { fetcher, isBuyer, isSeller, styles } from "../lib/utils";

const BasketTable = ({ items, removeFromBasket, canRemove = false }) => (
  <div className="overflow-x-scroll lg:overflow-x-hidden">
    <table className="w-full mt-8 bg-white border border-gray-200 rounded table-auto min-w-max">
      <thead>
        <tr className="text-sm leading-normal text-gray-600 uppercase bg-gray-200">
          <th className="p-3 pl-6 text-left">Product name</th>
          <th className="p-3 text-center">Count</th>
          <th className="p-3 text-center">Total price</th>
          {canRemove && <th className="p-3 pr-6 text-right">Actions</th>}
        </tr>
      </thead>
      <tbody className="text-sm font-light text-gray-600 ">
        {items.map((item, idx) => (
          <tr
            key={item.id}
            className={`border-b border-gray-200 hover:bg-gray-100 ${
              idx % 2 === 0 ? "bg-gray-50" : ""
            }`}
          >
            <td className="p-3 pl-6 text-left whitespace-nowrap">
              <div className="flex items-center underline">
                <Link href={`/products/${item.product_id}`}>
                  <a className="font-medium">{item.name}</a>
                </Link>
              </div>
            </td>
            <td className="p-3 font-bold text-center">
              <span className="px-3 py-1 bg-green-500 rounded-full text-gray-50">
                {item.count}
              </span>
            </td>
            <td className="p-3 text-center">
              <span className="px-3 py-1 font-bold bg-purple-600 rounded-full text-gray-50">
                {Math.floor(item.count * item.price * 100) / 100}
              </span>
            </td>
            {canRemove && (
              <td className="flex justify-end p-3 pr-6 item-center">
                <div className="w-6 mr-2 transform hover:text-purple-500 hover:scale-110 hover:cursor-pointer">
                  <TrashIcon
                    onClick={async () => await removeFromBasket(item.id)}
                  />
                </div>
              </td>
            )}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

const Profile = () => {
  const { user } = useUser({ redirectTo: "/login" });
  const [products, setProducts] = useState([]);
  const [toast, setToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const { data: organisation, error: orgErr } = useSWR(
    user?.isFull && user.organisation_id
      ? `${baseUrl}/organisation/${user.organisation_id}`
      : null,
    fetcher
  );

  const { data: campaigns, error: campaignsError } = useSWR(
    user?.isFull && isSeller(user) && user.organisation_id
      ? `${baseUrl}/campaign/organisation/${user.organisation_id}`
      : null,
    fetcher
  );

  const { data: basket, error: basketError } = useSWR(
    user && user.isFull && isBuyer(user) ? `${baseUrl}/basket` : null,
    (e) => fetcher(e, { headers: { Authorization: `Bearer ${user.token}` } }),
    {
      onSuccess: async (data) => {
        setProducts(
          await Promise.all(
            Array.from(new Set(data.map((e) => e.product_id))).map((e) =>
              fetcher(`${baseUrl}/product/${e}`)
            )
          )
        );
      },
    }
  );

  const purchase = async () => {
    try {
      await basket
        .filter((e) => e.purchased === false)
        .map(({ count, id }) =>
          fetcher(`${baseUrl}/basket/${id}`, {
            method: "PUT",
            headers: { Authorization: `Bearer ${user.token}` },
            body: {
              count,
              purchased: true,
            },
          })
        );

      setToast("Items purchased successfully.");
      Router.push(`/payment`);
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  }

  const removeFromBasket = async (id) => {
    try {
      await fetcher(
        `${baseUrl}/basket/${id}`,
        {
          method: "DELETE",
          headers: { Authorization: `Bearer ${user.token}` },
        }
      );

      mutate(`${baseUrl}/basket`);

      setToast("Item removed from basket successfully.");
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  const logout = async (e) => {
    e.preventDefault();
    await fetcher("/api/logout", { method: "POST" });
    Router.push("/login");
  };

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>Handmade Devops - Profile Page</title>
      </Head>
      <div className={styles.card}>
        {!user?.isLoggedIn ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : user.isFull ? (
          <>
            <h1 className={styles.cardH1}>
              Hello, <span className="text-purple-800">{user.username}</span>!{" "}
              <span onClick={logout}>
                <Button size="small">Logout</Button>
              </span>
            </h1>
            <div className="w-full mt-4 text-lg">
              <p>You are a {user.user_role.toLowerCase()}.</p>

              {isBuyer(user) && (
                <div className="mt-2">Get yourself something nice.</div>
              )}

              {isSeller(user) &&
                (user?.organisation_id ? (
                  orgErr ? (
                    <div>We couldn't find your organisation</div>
                  ) : !organisation ? (
                    <div>Looking for your organisation...</div>
                  ) : (
                    <div className="mt-2">
                      Edit your organisation:{" "}
                      <Link
                        href={`/organisations/edit/${organisation.id}`}
                        passHref
                      >
                        <Button variant="green" size="small">
                          {organisation.name}
                        </Button>
                      </Link>
                    </div>
                  )
                ) : (
                  <Link href="/organisations/edit/new" passHref>
                    <Button variant="green" size="small">
                      Create an organisation
                    </Button>
                  </Link>
                ))}
            </div>
          </>
        ) : null}
      </div>
      {user?.isFull && isSeller(user) && user.organisation_id && (
        <div className={`${styles.card} max-w-7xl`}>
          {campaignsError ? (
            <div>We couldn't find your campaigns</div>
          ) : !campaigns ? (
            <h1 className={styles.cardH1}>Loading...</h1>
          ) : (
            <>
              <h1 className={styles.cardGridHeader}>Edit your campaigns</h1>
              <div className={styles.grid}>
                {campaigns.map((o) => (
                  <Card key={o.id} {...o} linkTo="campaigns/edit" />
                ))}
              </div>
              <Link href="/campaigns/edit/new" passHref>
                <Button
                  variant="green"
                  size="small"
                  className="absolute uppercase top-4 right-4"
                >
                  Create a new campaign
                </Button>
              </Link>
            </>
          )}
        </div>
      )}
      {user?.isFull && isBuyer(user) && (
        <div className={`${styles.card} max-w-7xl`}>
          {basketError ? (
            <div>We couldn't find your basket</div>
          ) : !basket || !products.length ? (
            <h1 className={styles.cardH1}>Loading...</h1>
          ) : (
            <>
              <h1 className={styles.cardGridHeader}>Your basket</h1>
              <BasketTable
                items={basket
                  .filter((e) => e.purchased === false)
                  .map((o) => {
                    const { name, price } = products.find(
                      (e) => e.id === o.product_id
                    );
                    return { ...o, name, price };
                  })}
                removeFromBasket={removeFromBasket}
                canRemove={true}
              />
              {toast && (
                <div className="flex justify-end my-4">
                  <Toast message={toast} show={toast} setShow={setToast} />
                </div>
              )}
              {errorMsg && (
                <div className="my-4 font-bold text-red-600">
                  <p className="my-4">{errorMsg}</p>
                </div>
              )}
              <div className="flex justify-end my-4">
                <Button onClick={purchase}>Buy</Button>
              </div>
              <h2 className="mt-16 text-xl font-bold text-center lg:text-3xl">
                Purchased items
              </h2>
              <BasketTable
                items={basket
                  .filter((e) => e.purchased === true)
                  .map((o) => {
                    const { name, price } = products.find(
                      (e) => e.id === o.product_id
                    );
                    return { ...o, name, price };
                  })}
                removeFromBasket={removeFromBasket}
              />
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default Profile;
