import Head from "next/head";
import { useRouter } from "next/router";
import useSWR from "swr";
import Card from "../../components/Card";
import { baseUrl } from "../../lib/config";
import { fetcher, styles } from "../../lib/utils";

const Organisation = () => {
  const router = useRouter();
  const { slug } = router.query;

  const { data: organisation, error } = useSWR(
    slug ? `${baseUrl}/organisation/${slug}` : null,
    fetcher
  );

  const { data: campaigns, error: campaignsError } = useSWR(
    organisation ? `${baseUrl}/campaign/organisation/${organisation.id}` : null,
    fetcher
  );

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>
          {organisation
            ? `Handmade Devops - ${organisation.name}`
            : "Organisation loading..."}
        </title>
      </Head>
      <div className={styles.card}>
        {error ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : !organisation ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (
          <div className="items-center md:flex">
            <div className="w-full mb-10 md:px-10 md:w-1/2 md:mb-0">
              <div className="relative min-h-[20em]">
                <img src="" className="relative z-10 w-full" alt="" />
                <div className="absolute z-0 border-4 border-purple-600 top-10 bottom-10 left-10 right-10"></div>
              </div>
            </div>
            <div className="w-full md:px-10 md:w-1/2">
              <div className="mb-10">
                <h1 className="mb-5 text-2xl font-bold uppercase">
                  {organisation.name}
                </h1>
                <p className="text-sm">{organisation.description}</p>
              </div>
            </div>
          </div>
        )}
      </div>

      <div className={`${styles.card} max-w-7xl`}>
        <h1 className={styles.cardGridHeader}>Organisation campaigns</h1>
        {campaignsError ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : !campaigns ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : (
          <div className={styles.grid}>
            {campaigns.map((o) => (
              <Card key={o.id} {...o} linkTo="campaigns" />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Organisation;
