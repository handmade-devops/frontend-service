import Head from "next/head";
import Router, { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import useSWR from "swr";
import Button from "../../../components/Button";
import Toast from "../../../components/Toast";
import { baseUrl } from "../../../lib/config";
import useUser from "../../../lib/useUser";
import { fetcher, isBuyer, styles } from "../../../lib/utils";

const EditOrganisation = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const [toast, setToast] = useState(false);
  const router = useRouter();
  const { slug } = router.query;
  const isNew = slug === "new";
  const { user } = useUser({ redirectTo: "/login" });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const { data: org, error } = useSWR(
    user?.isFull && slug == user?.organisation_id
      ? `${baseUrl}/organisation/${user.organisation_id}`
      : null,
    fetcher
  );

  const onSubmit = async (body) => {
    try {
      await fetcher(
        `${baseUrl}/organisation/${isNew ? "" : user?.organisation_id}`,
        {
          method: isNew ? "POST" : "PUT",
          headers: { Authorization: `Bearer ${user.token}` },
          body,
        }
      );

      setToast("Data saved successfully.");
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  const deleteOrganisation = async () => {
    try {
      await fetcher(`${baseUrl}/organisation/${slug}`, {
        method: "DELETE",
        headers: { Authorization: `Bearer ${user.token}` },
      });

      setToast("Data deleted successfully.");
      Router.push(`/profile`);
    } catch (error) {
      setErrorMsg(error.data.message);
    }
  };

  if (user && !user?.isLoggedIn) {
    Router.push("/login");
  } else if (
    isBuyer(user) ||
    (isNew ? user?.organisation_id !== null : slug != user?.organisation_id)
  ) {
    Router.push("/profile");
  }

  return (
    <div className={styles.composedPage}>
      <Head>
        <title>
          Handmade Devops - {isNew ? "Create" : "Edit"} organisation
        </title>
      </Head>
      <div className={styles.card}>
        {!user || !user.isFull ? (
          <h1 className={styles.cardH1}>Loading...</h1>
        ) : isNew || org ? (
          <>
            <h1 className={styles.cardH1}>
              {isNew ? "Create" : "Edit"} organisation
            </h1>
            <form onSubmit={handleSubmit(onSubmit)} className="w-full my-4">
              <div className="flex flex-col mb-4 font-bold">
                <label>
                  Name *
                  <input
                    type="text"
                    name="name"
                    placeholder="name"
                    defaultValue={isNew ? "" : org.name}
                    required
                    className={styles.input}
                    {...register("name", { required: true })}
                  />
                </label>
                <label>
                  Description
                  <textarea
                    type="text"
                    name="description"
                    placeholder="description"
                    defaultValue={isNew ? "" : org.description}
                    className={styles.input}
                    {...register("description")}
                  />
                </label>
              </div>

              <div className="font-bold text-red-600">
                <p className="my-4">{errors.name && "Name is required"}</p>
                <p className="my-4">{errorMsg && errorMsg}</p>
              </div>
              <Toast message={toast} show={toast} setShow={setToast} />

              <div className="flex flex-row flex-wrap justify-between gap-4">
                <Button type="submit" onClick={handleSubmit(onSubmit)}>
                  Save
                </Button>

                {!isNew && (
                  <Button onClick={deleteOrganisation} variant="red" noArrow>
                    Delete
                  </Button>
                )}
              </div>
            </form>
          </>
        ) : error ? (
          <h1 className={styles.cardH1}>An error has occured</h1>
        ) : (
          <h1 className={styles.cardH1}>Loading...</h1>
        )}
      </div>
    </div>
  );
};

export default EditOrganisation;
