import { apiUrl } from "../../lib/config";
import { fetcher } from "../../lib/utils";
import withSession from "../../lib/session";

export default withSession(async (req, res) => {
  try {
    const user = await fetcher(`${apiUrl}/user`, {
      method: "POST",
      body: req.body,
    });

    req.session.set("user", { isLoggedIn: true, ...user });
    await req.session.save();
    res.json(user);
  } catch (error) {
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(error.data);
  }
});
