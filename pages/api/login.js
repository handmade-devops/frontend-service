import { apiUrl } from "../../lib/config";
import { fetcher } from "../../lib/utils";
import withSession from "../../lib/session";

export default withSession(async (req, res) => {
  try {
    const { token, refreshToken } = await fetcher(`${apiUrl}/auth`, {
      method: "POST",
      body: req.body,
    });

    req.session.set("user", { isLoggedIn: true, token, refreshToken });
    await req.session.save();
    res.json(req.session.get("user"));
  } catch (error) {
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(error.data);
  }
});
