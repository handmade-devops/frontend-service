import { apiUrl } from "../../lib/config";
import withSession from "../../lib/session";
import { fetcher } from "../../lib/utils";

const exitEndpoint = (req, res) => {
  req.session.destroy();
  res.json({ isLoggedIn: false });
};

export default withSession(async (req, res) => {
  const user = req.session.get("user");
  if (!user) return exitEndpoint(req, res);

  let { token, refreshToken } = user;
  // If tokens are missing, exit
  if (!token || !refreshToken) return exitEndpoint(req, res);

  try {
    // Request user data
    let response = await fetcher(`${apiUrl}/user`, {
      headers: { Authorization: `Bearer ${token}` },
    });

    if (response === null) {
      // Auth token has expired, try refreshing it
      const tokens = await fetcher(`${apiUrl}/auth/refresh`, {
        method: "POST",
        headers: { Authorization: `Bearer ${token}` },
        body: { refreshToken },
      });

      // refreshing tokens didn't work, destroy session
      if (tokens === null) return exitEndpoint(req, res);

      token = tokens.token;
      refreshToken = tokens.refreshToken;
      req.session.set("user", { isLoggedIn: true, token, refreshToken });
      await req.session.save();

      // retry getting user data
      response = await fetcher(`${apiUrl}/user`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    }

    res.json({ isLoggedIn: true, isFull: true, ...user, ...response });
  } catch (err) {
    if (!err.data) err.data = { message: err.message };
    throw err;
  }
});
