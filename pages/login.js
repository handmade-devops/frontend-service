import Head from "next/head";
import Link from "next/link";
import { useState } from "react";
import Button from "../components/Button";
import useUser from "../lib/useUser";
import { fetcher, styles } from "../lib/utils";

const Login = () => {
  const { mutateUser } = useUser({
    redirectTo: "/profile",
    redirectIfFound: true,
  });

  const [formName, setFormName] = useState("");
  const [formPassword, setFormPassword] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  const handleFormChange = (e) => {
    const target = e.target;
    target.name === "name"
      ? setFormName(target.value)
      : setFormPassword(target.value);
  };

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      await mutateUser(
        fetcher("/api/login", {
          method: "POST",
          body: { username: formName, password: formPassword },
        })
      );
    } catch (error) {
      console.error("An unexpected error happened:", error);
      setErrorMsg(error.data.message);
    }
  }

  return (
    <div className={styles.page}>
      <Head>
        <title>Handmade - Login</title>
      </Head>
      <h1 className={styles.cardGridHeader}>Let's log you in</h1>
      <form
        onSubmit={handleSubmit}
        className="w-full max-w-xl p-4 mx-8 mt-4 mb-16 text-gray-800 border-2 border-purple-600 rounded-xl"
      >
        <div className="flex flex-col mb-4 font-bold">
          <label>
            Username *
            <input
              type="text"
              name="name"
              placeholder="username"
              autoComplete="username"
              required
              className={styles.input}
              value={formName}
              onChange={handleFormChange}
            />
          </label>
          <label>
            Your password *
            <input
              type="password"
              name="current-password"
              placeholder="password"
              autoComplete="current-password"
              required
              className={styles.input}
              value={formPassword}
              onChange={handleFormChange}
            />
          </label>
        </div>

        <div className="font-bold text-red-600">
          <p className="my-4">{errorMsg && errorMsg}</p>
        </div>

        <Button type="submit" onClick={handleSubmit}>
          Login
        </Button>

        <div className="flex items-center justify-end gap-2 my-2">
          <span>Don't have an account yet?</span>{" "}
          <Link href="/register" passHref>
            <Button size="small" variant="green">
              Register here!
            </Button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default Login;
