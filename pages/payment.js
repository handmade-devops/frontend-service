import Link from "next/link";
import { styles } from "../lib/utils";

const Payment = () => {
  return (
    <div className={styles.composedPage}>
      <div className="w-full max-w-2xl p-5 mx-auto text-gray-700 bg-white rounded-lg shadow-lg">
        <div className="w-full pt-1 pb-5">
          <div className="flex items-center justify-center w-20 h-20 mx-auto -mt-16 overflow-hidden text-white bg-purple-600 rounded-full shadow-lg">
            <i className="text-3xl mdi mdi-credit-card-outline"></i>
          </div>
        </div>
        <div className="mb-10">
          <h1 className="text-xl font-bold text-center uppercase">
            Secure payment info
          </h1>
        </div>
        <div className="flex mb-3 -mx-2">
          <div className="px-2">
            <label htmlFor="type1" className="flex items-center cursor-pointer">
              <input
                type="radio"
                className="w-5 h-5 text-indigo-500 form-radio"
                name="type"
                id="type1"
                defaultChecked
              />
              <img src="assets/images/cards.png" className="h-8 ml-3" />
            </label>
          </div>
          <div className="px-2">
            <label htmlFor="type2" className="flex items-center cursor-pointer">
              <input
                type="radio"
                className="w-5 h-5 text-indigo-500 form-radio"
                name="type"
                id="type2"
              />
              <img src="assets/images/paypal.png" className="h-8 ml-3" />
            </label>
          </div>
        </div>
        <div className="mb-3">
          <label className="mb-2 ml-1 text-sm font-bold">Name on card</label>
          <div>
            <input
              className="w-full px-3 py-2 mb-1 transition-colors border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500"
              placeholder="John Smith"
              type="text"
            />
          </div>
        </div>
        <div className="mb-3">
          <label className="mb-2 ml-1 text-sm font-bold">Card number</label>
          <div>
            <input
              className="w-full px-3 py-2 mb-1 transition-colors border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500"
              placeholder="0000 0000 0000 0000"
              type="text"
            />
          </div>
        </div>
        <div className="flex items-end mb-3 -mx-2">
          <div className="w-1/2 px-2">
            <label className="mb-2 ml-1 text-sm font-bold">
              Expiration date
            </label>
            <div>
              <select className="w-full px-3 py-2 mb-1 transition-colors border-2 border-gray-200 rounded-md cursor-pointer form-select focus:outline-none focus:border-indigo-500">
                <option value="01">01 - January</option>
                <option value="02">02 - February</option>
                <option value="03">03 - March</option>
                <option value="04">04 - April</option>
                <option value="05">05 - May</option>
                <option value="06">06 - June</option>
                <option value="07">07 - July</option>
                <option value="08">08 - August</option>
                <option value="09">09 - September</option>
                <option value="10">10 - October</option>
                <option value="11">11 - November</option>
                <option value="12">12 - December</option>
              </select>
            </div>
          </div>
          <div className="w-1/2 px-2">
            <select className="w-full px-3 py-2 mb-1 transition-colors border-2 border-gray-200 rounded-md cursor-pointer form-select focus:outline-none focus:border-indigo-500">
              <option value="2020">2020</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
              <option value="2023">2023</option>
              <option value="2024">2024</option>
              <option value="2025">2025</option>
              <option value="2026">2026</option>
              <option value="2027">2027</option>
              <option value="2028">2028</option>
              <option value="2029">2029</option>
            </select>
          </div>
        </div>
        <div className="mb-10">
          <label className="mb-2 ml-1 text-sm font-bold">Security code</label>
          <div>
            <input
              className="w-32 px-3 py-2 mb-1 transition-colors border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500"
              placeholder="000"
              type="text"
            />
          </div>
        </div>
        <div>
          <Link href="/profile" passHref>
            <button className="block w-full max-w-xs px-3 py-3 mx-auto font-semibold text-white bg-indigo-500 rounded-lg hover:bg-indigo-700 focus:bg-indigo-700">
              <i className="mr-1 mdi mdi-lock-outline"></i> PAY NOW
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Payment;
